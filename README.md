## README ##

This repository contains R code and data to reproduces figures in the book "P-splines. The Craft of Smoothing", written by Paul Eilers and Brian Marx. The book is not yet finished, but here we already offer software to give prospective readers a taste of what they can expect. Keep in mind that this is work in progress and that the contents can (and will) change without warning. 

### How do I get set up? ###

Clone this repository somwhere on your computer. Three directories will be created.

* Scripts. Here you wil find the R-programs that make the graphs. Their name always start with 'f_'. There are a few support files in the same directory.

* Data. Here you wil find the data that are needed by some of the programs.

* Graphs. Each program produces a PDF file. To avoid clutter, these are placed in this directory automatically.

A recent version of R is needed, but the version number is not critical. Each script uses the *ggplot2* package and many use *gridExtra* and *colorspace*. Additional packages (like *spam*) are used by some of the scripts.


### Who do I talk to? ###

* Send comments to Paul Eilers (p.eilers@erasmusmc.nl) or Brian Marx (bdmarx@lsu.edu)
