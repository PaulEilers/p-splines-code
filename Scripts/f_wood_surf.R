# Wood surface

library(ggplot2)
library(spam)

source('craft_support.R')
Y = read.table('../Data/woodsurf.prn')
y = Y[,1]
m = length(y)
x = 1:m
w = 0 * y + 1

# Prepare for smoothing
E = diag.spam(m)
D = diff(E, diff = 2)

lambda = 2.8e6
E = diag(m)
D = diff(E, diff = 2)
P = lambda * t(D) %*% D 
z = solve(E + P, y)
y = Y[,1]
m = length(y)
x = 1:m
w = 0 * y + 1


# Prepare for smoothing
E = diag.spam(m)
D = diff(E, diff = 2)
F1 = data.frame(x = x, y = z)


pl = qplot(x, y, colour = I('blue'))  +
     geom_line(data = F1, color = I('red'), size = 1, lty = 1) +
     geom_line(colour = I('blue')) +
     xlab('Position') + ylab('') +
     ggtitle('Wood surface') +
     cos_theme()

set_window(height = 4)
print(pl)


save_PDF('f_wood_surf')

